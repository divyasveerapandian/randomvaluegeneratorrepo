package generator;
import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
public class RandomValueGenerator {
	
	public static void main(String[] args) {
		RandomValueGenerator randomVG = new RandomValueGenerator();
		try {
			//Call Random numbers generating Method
			int [] data =randomVG.randomInteger(5,1,10,1,"plain","new");
			for (int i = 0; i < data.length; i++) {
				System.out.print(data[i]+" ");
			}
			//Call to generate the Bit map Image
			randomVG.generateBitMap();
			
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	/**Random numbers generating function
	 * Input parameters
	 *  int num  - total number of random numbers generated
	 *  int min  - minimum value The lower bound of the interval (inclusive).
	 *  int max  - The upper bound of the interval
	 *  int col  - The number of columns in which the integers will be arranged. The integers should be read (or processed) left to right across columns.
	 *  String format - Determines the return type of the document that the server produces as its response (PLAIN TEXT)
	 *  String rnd - Determines the randomization to use to generate the sequence.
	 */
	private int[] randomInteger(int num, int min, int max, int col, String format, String rnd) throws IOException {
		//Used the HTTP API of random.org for random number generation
		String url ="https://www.random.org/integers/?num="+num+"&min="+min+"&max="+max+"&col="+col
				+"&base=10&format="+format+"&rnd="+rnd;
		InputStream in = apiConnection(url).getInputStream();
		BufferedReader res = new BufferedReader(new InputStreamReader(in, "UTF-8"));
		int[] arr = new int[num];
		int i=0;
		String inputLine;
		while ((inputLine = res.readLine()) != null){
			arr[i]= Integer.parseInt(inputLine);
			i++;
		}
		res.close();
		return arr;
	}
	/**
	 * Generates the Bit map Image of size 128*128
	 */
	public void generateBitMap() throws IOException {
		BufferedImage img = createImage( 128, 128);
		JLabel jLabel = new JLabel(new ImageIcon(img));
		JPanel jPanel = new JPanel();
		jPanel.add(jLabel);
		JFrame r = new JFrame();
		r.add(jPanel);
		r.setVisible(true);
		r.setSize(500, 500);
	}
	/**
	 * Image creation with random numbers generated
	 */
	private BufferedImage createImage( int sizeX, int sizeY ) throws IOException{
		final BufferedImage res = new BufferedImage( sizeX, sizeY, BufferedImage.TYPE_INT_RGB );
		int[] data = null;
		data = randomInteger(sizeY,1,sizeX,1,"plain","new");
		for (int x = 0; x < data.length; x=x+2){
			res.setRGB(data[x], data[x+1], Color.WHITE.getRGB());
		}
		return res;
	}
	/**
	 * API Request Method
	 */
	public HttpURLConnection apiConnection(String url) throws IOException  {
		HttpURLConnection httpcon = (HttpURLConnection) ((new URL(url).openConnection()));
		httpcon.setDoOutput(true);
		httpcon.setRequestProperty("Content-Type", "application/json");
		httpcon.setRequestProperty("Accept", "application/json");
		httpcon.setRequestMethod("GET");
		httpcon.connect();
		return httpcon;
	}

}
